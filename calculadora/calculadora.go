package main

import (
	"fmt"
	"goTest/calc"
)

func main() {
	nome := "Ezequiel"
	fmt.Println("Olá", nome)

	fmt.Println("1- SOMAR")
	fmt.Println("2- SUBTRAIR")
	fmt.Println("3- DIVIDIR")
	fmt.Println("4- MULTIPLICAR")

	var comando int
	fmt.Scan(&comando)

	switch comando {
	case 1:
		fmt.Println("Iniciando SOMA")
		result := calc.Soma()
		fmt.Println("Resultado:", result)
	case 2:
		fmt.Println("Iniciando SUBTRACAO")
		result := calc.Sub()
		fmt.Println("Resultado:", result)
	case 3:
		fmt.Println("Iniciando DIVISAO")
		result := calc.Div()
		fmt.Println("Resultado:", result)
	case 4:
		fmt.Println("Iniciando MULTIPLICACAO")
		result := calc.Mult()
		fmt.Println("Resultado:", result)
	default:
		fmt.Println("Default")
	}
}
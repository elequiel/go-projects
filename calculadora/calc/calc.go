package calc

import "fmt"

// Retorna 2 valores
func getNumbers() (int, int) {
	var num1 int
	var num2 int
	fmt.Printf("Insira o primeiro numero: ")
	fmt.Scan(&num1)

	fmt.Printf("Insira o segundo numero: ")
	fmt.Scan(&num2)

	return num1, num2
}

func Soma() int {
	num1, num2 := getNumbers()
	return num1 + num2
}

func Sub() int {
	num1, num2 := getNumbers()
	return num1 - num2
}

func Div() int {
	num1, num2 := getNumbers()
	if num2 == 0 {
		return -1
	}
	return num1 / num2
}

func Mult() int {
	num1, num2 := getNumbers()
	return num1 * num2
}
